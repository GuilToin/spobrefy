 (função ($) {

     // Definindo nosso plugin jQuery

     $ .fn.pointPoint = function (prop) {

         // Parâmetros padrão

         var options = $ .extend ({
             "class": "pointPointArrow",
             "distância": 30
         }, prop);

         var ponteiros = [];

         // Se as transformações CSS não forem suportadas, saia;

         if (! $. support.transform) {
             this.destroyPointPoint = function () {};
             devolva isto;
         }

         this.each (function () {

             var findMe = $ (this),
                 point = $ ('<div class = "' + opções ['class'] + '">'). appendTo ('body'),
                 deslocamento, centro = {}, mouse = {}, adereços = {}, a, b, h, deg, op,
                 pointHidden = true, rad_to_deg = 180 / Math.PI;

             ponteiros.push (ponto);

             // Calculando a posição do ponteiro no movimento do mouse

             $ ('html'). bind ('mousemove.pointPoint', função (e) {

                 if (pointHidden) {
                     point.show ();
                     pointHidden = false;
                 }

                 deslocamento = findMe.offset ();

                 // O centro do elemento para o qual estamos apontando
                 center.x = offset.left + findMe.outerWidth () / 2;
                 center.y = offset.top + findMe.outerHeight () / 2;

                 mouse.x = e.pageX;
                 mouse.y = e.pageY;

                 // Estamos tratando a posição do mouse e o centro
                 // aponta como os cantos de um triângulo retângulo.
                 // h é a hipotenusa, ou a distância entre os dois.

                 a = mouse.y - center.y;
                 b = center.x - mouse.x;
                 h = Math.sqrt (a * a + b * b);

                 // Calculando o grau (em radianos),
                 // o ponteiro deve ser girado por:
                 deg = Math.atan2 (a, b);

                 // Diminuindo a opacidade do ponteiro, dependendo
                 // na distância do ponteiro do mouse

                 op = 1;
                 if (h <50) {
                     op = 0;
                 } else if (h <160) {
                     op = (h - 50) / 110;
                 }

                 // Movendo e girando o ponteiro

                 props.marginTop = mouse.y-options.distance * Math.sin (deg);
                 props.marginLeft = mouse.x + options.distance * Math.cos (deg);
                 props.transform = 'rotate (' + (- deg * rad_to_deg) + 'deg)';
                 props.opacity = op;

                 point.css (props);

             }) bind ('mouseleave.pointPoint', function () {
                 point.hide ();
                 pointHidden = true;
             });

         });

         this.destroyPointPoint = function () {

             // Desvincular todos os manipuladores de eventos
             // e remova () os ponteiros 

             $ ('html'). unbind ('. pointPoint');

             $ .each (ponteiros, função () {
                 this.remove ();
             });

         };

         devolva isto;
     };

 }) (jQuery); 